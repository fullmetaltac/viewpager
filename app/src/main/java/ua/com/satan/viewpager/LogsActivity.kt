package ua.com.satan.viewpager

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_logs.*


class LogsActivity : AppCompatActivity(), ControlsFragment.OnClearLogsListener {
    private lateinit var logsFragment: LogsFragment
    private lateinit var controlsFragment: ControlsFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logs)

        logsFragment = LogsFragment()
        controlsFragment = ControlsFragment()

        supportFragmentManager.beginTransaction().also {
            it.add(frame_layout_logs.id, logsFragment)
            it.add(frame_layout_controls.id, controlsFragment)
        }.commit()
    }

    override fun clearLogs() {
        logsFragment.setLogsText("")
    }
}