package ua.com.satan.viewpager

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_page.*
import java.util.*
import java.util.concurrent.TimeUnit


class PageFragment : Fragment() {

    private val handler: Handler = Handler()
    private var setColorTask = Runnable { button.setBackgroundColor(randomColor().also { state.color = it }) }

    private lateinit var state: FragmntState

    companion object {
        fun newInstance(page: Int): PageFragment {
            val fragment = PageFragment()

            val args = Bundle()
            args.putInt("pageNum", page)
            fragment.arguments = args

            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        val pageNum = arguments.getInt("pageNum")
        state = FragmntState(pageNum, ContextCompat.getColor(context, R.color.btnDefault))
        Log.d(state.tag, "onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(state.tag, "onCreate")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        Log.d(state.tag, "onCreateView")
        return inflater!!.inflate(R.layout.fragment_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(state.tag, "onViewCreated")
        textLabel.text = resources.getString(R.string.pageTitle, state.page)
        button.setOnClickListener { changeButtonColor() }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.d(state.tag, "onActivityCreated")
        if (savedInstanceState != null) {
            state = savedInstanceState.getSerializable("state") as FragmntState
            count.text = "${state.count}"
            button.setBackgroundColor(state.color)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d(state.tag, "onSaveInstanceState")
        outState.putSerializable("state", state)
    }

    override fun onStart() {
        super.onStart()
        Log.d(state.tag, "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(state.tag, "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(state.tag, "onPause")
    }

    override fun onStop() {
        super.onStop()
        handler.removeCallbacks(setColorTask)
        Log.d(state.tag, "onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(state.tag, "onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(state.tag, "onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Log.d(state.tag, "onDetach")
    }

    private fun changeButtonColor() {
        count.text = "${++state.count}"
        handler.postDelayed(setColorTask, TimeUnit.SECONDS.toMillis(state.count))
    }

    private fun randomColor() = Color.argb(
            255,
            Random().nextInt(256),
            Random().nextInt(256),
            Random().nextInt(256)
    )
}