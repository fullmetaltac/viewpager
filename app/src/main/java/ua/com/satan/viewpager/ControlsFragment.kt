package ua.com.satan.viewpager

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_controls.*

class ControlsFragment : Fragment() {

    private lateinit var onClearLogsListener: OnClearLogsListener

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_controls, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clear.setOnClickListener { onClearLogsListener.clearLogs() }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onClearLogsListener = activity as OnClearLogsListener
    }

    interface OnClearLogsListener {
        fun clearLogs()
    }
}