package ua.com.satan.viewpager

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_logs.*
import java.io.BufferedReader
import java.io.InputStreamReader

class LogsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater!!.inflate(R.layout.fragment_logs, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logs.movementMethod = ScrollingMovementMethod()
        setLogsText(getLogs().also { saveToFile(context, it) })
    }

    fun setLogsText(text: String) {
        logs.text = text
    }

    private fun getLogs(): String {
        val process = Runtime.getRuntime().exec("logcat -d")
        return BufferedReader(InputStreamReader(process.inputStream))
                .readText()
                .split('\n')
                .filter { it.contains("ViewPager@") }
                .joinToString("\n") { it.split('@').last() }
    }

    private fun saveToFile(context: Context?, logs: String, fileName: String = "ViewPagerLogs") =
            context?.openFileOutput(fileName, Context.MODE_PRIVATE).use { it?.write(logs.toByteArray()) }
}