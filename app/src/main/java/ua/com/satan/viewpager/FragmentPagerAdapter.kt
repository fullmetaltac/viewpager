package ua.com.satan.viewpager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

class FragmentPagerAdapter(fragmentManager: FragmentManager) : StatePagerAdapter<Fragment>(fragmentManager) {
    companion object {
        private const val numItems = 10
    }

    private var fragments: List<PageFragment> = (1..10).map { PageFragment.newInstance(it) }

    override fun getCount() = numItems

    override fun getItem(position: Int) = fragments[position]
}