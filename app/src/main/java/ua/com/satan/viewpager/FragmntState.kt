package ua.com.satan.viewpager

import java.io.Serializable

class FragmntState(
        var page: Int,
        var color: Int,
        var count: Long = 0,
        var tag: String = "ViewPager@Fragment#$page"
) : Serializable